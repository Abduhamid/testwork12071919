<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Api\ClientController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/auth/login',  [AuthController::class ,'login']);

Route::group([
 'middleware' => 'auth:api',
], function ($router) {

    Route::post('/auth/logout',  [AuthController::class ,'logout']);
    Route::post('/auth/refresh',  [AuthController::class ,'refresh']);
    Route::post('/auth/me',  [AuthController::class ,'me']);
    Route::resource('clients', ClientController::class, ['except' => ['create']]);

});
