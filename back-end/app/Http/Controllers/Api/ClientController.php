<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Client\ClientResource;
use App\Repositories\Client\ClientRepositoryContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    protected $clientRepository;

    public function __construct(ClientRepositoryContract $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $clients = ClientResource::collection($this->clientRepository->all());
            return response()->json($clients, 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), $e->getTrace());
            return response()->json([
                'error'=>'server-internal-error',
                'message'=>"Непредвиденная ошибка сервера",
            ], 500);
        }
    }


    protected function createClientValidator(array $data)
    {
        return Validator::make($data, [
            "email" => 'required|email|unique:clients,email',
            "surname" => 'required|string',
            "name" => 'required|string',
            "patronymic" => 'sometimes|string|nullable',
            "phone" => 'required|numeric|digits_between:11,20',
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = $this->createClientValidator($request->all());
            $messages = $validator->errors();
            if (!$messages->isEmpty()) {
                return response()->json([
                    'error'=>'not-correct-input',
                    'message'=> $messages,
                ], 422);
            }
            $data = $request->all();
            $data['phone'] = $this->validatePhone($request->phone);
            $client = $this->clientRepository->store($data);
            return response()->json(ClientResource::make($client), 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), $e->getTrace());
            return response()->json([
                'error'=>'server-internal-error',
                'message'=>"Непредвиденная ошибка сервера",
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        try {
            $client = ClientResource::make($this->clientRepository->getById($id));
            return response()->json($client, 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), $e->getTrace());
            return response()->json([
                'error'=>'server-internal-error',
                'message'=>"Непредвиденная ошибка сервера",
            ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $client = ClientResource::make($this->clientRepository->getById($id));
            return response()->json($client, 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), $e->getTrace());
            return response()->json([
                'error'=>'server-internal-error',
                'message'=>"Непредвиденная ошибка сервера",
            ], 500);
        }
    }

    protected function editClientValidator(array $data)
    {
        return Validator::make($data, [
            "surname" => 'sometimes|string',
            "name" => 'sometimes|string',
            "patronymic" => 'sometimes|string|nullable',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        try {
            $validator = $this->editClientValidator($request->all());
            $messages = $validator->errors();
            if (!$messages->isEmpty()) {
                return response()->json([
                    'error'=>'not-correct-input',
                    'message'=> $messages,
                ], 422);
            }
            $inputs = [
                'surname',
                'name',
                'patronymic',
            ];
            $data = [];
            foreach ($inputs as $input) {
                if ($request->has($input)) {
                    $data[$input] = $request->$input;
                }
            }
            return response()->json($this->clientRepository->update($id, $data), 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), $e->getTrace());
            return response()->json([
                'error'=>'server-internal-error',
                'message'=>"Непредвиденная ошибка сервера",
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        try {
            $this->clientRepository->destroy($id);

            return response()->json([
                'status' => 'success'
            ], 204);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), $e->getTrace());
            return response()->json([
                'error'=>'server-internal-error',
                'message'=>"Непредвиденная ошибка сервера",
            ], 500);
        }
    }

    protected function validatePhone(string $phone)
    {
        $phoneStr = strval(preg_replace("/[^\d]/", "", $phone));
        if (strlen($phoneStr)==11) {
            $phoneStr[0]='7';
            return $phoneStr;
        } else {
            return preg_replace("/[^\d]/", "", $phone);
        }
    }

}
