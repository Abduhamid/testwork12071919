<?php

namespace App\Providers;

use App\Repositories\Client\ClientEloquentRepository;
use App\Repositories\Client\ClientRepositoryContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //binding repository
        $this->app->bind(ClientRepositoryContract::class, ClientEloquentRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
