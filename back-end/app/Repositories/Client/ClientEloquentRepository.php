<?php

namespace App\Repositories\Client;


use App\Models\Client;

class ClientEloquentRepository implements ClientRepositoryContract
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }
    public function all()
    {
        return $this->client->get();
    }

    public function getById(int $id)
    {
        return $this->client->findOrFail($id);
    }

    public function store(array $data)
    {
        return $this->client->create($data);
    }

    public function update(int $id, array $data)
    {
        $client = $this->getById($id);
        return $client->update($data);
    }

    public function destroy(int $id)
    {
        $client = $this->getById($id);
        return $client->delete();
    }
}