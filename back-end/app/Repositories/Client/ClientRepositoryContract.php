<?php

namespace App\Repositories\Client;

interface ClientRepositoryContract
{
    public function all();

    public function getById(int $id);

    public function store(array $data);

    public function update(int $id, array $data);

    public function destroy(int $id);
}