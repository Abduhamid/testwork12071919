<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'User',
                'email' => 'user@mail.ru',
                'password' => bcrypt('password'),
            ],
        ];

        foreach ($users as $user) {
            try{
                User::create($user);
            }catch (\Exception $e){

            }
        }
    }
}
