import Vue from 'vue';
import auth from '@websanova/vue-auth';
import axios from 'axios';
import VueAxios from 'vue-axios';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import 'semantic-ui-css/semantic.css';

import App from './App.vue';
import router from './router';
import services from './helpers/services';
import authConfig from './helpers/authConfig';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

axios.defaults.baseURL = process.env.VUE_APP_API_URL;
Vue.use(VueAxios, axios);

Vue.config.productionTip = false;

Vue.router = router;

Vue.use(auth, authConfig);

Vue.use(services);

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
