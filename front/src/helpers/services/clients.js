export default (api) => ({
  list: () => api.get('/clients'),
  delete: (id) => api.delete(`/clients/${id}`),
  get: (id) => api.get(`/clients/${id}`),
  edit: (id) => api.get(`/clients/${id}/edit`),
  update: (id, data) => api.patch(`/clients/${id}`, data),
  store: (data) => api.post('/clients', data),
});
