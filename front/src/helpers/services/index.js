import ClientssService from './clients';

const services = (Vue) => {
  const { $http } = Vue.prototype;

  return {
    clients: ClientssService($http),
  };
};

class Services {
  static install(Vue) {
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$services = services(Vue);
  }
}

export default Services;
