import httpDriver from '@websanova/vue-auth/drivers/http/axios.1.x';
import routerDriver from '@websanova/vue-auth/drivers/router/vue-router.2.x';

export default {
  auth: {
    request(req, token) {
      this.options.http.setHeaders.call(this, req, { Authorization: `Bearer ${token}`, mode: 'no-cors' });
    },

    response(res) {
      return (res.data || {}).access_token;
    },
  },
  http: httpDriver,
  router: routerDriver,
  rolesVar: 'role',
  authRedirect: { path: '/login' },
  forbiddenRedirect: { path: '/403' },
  notFoundRedirect: { path: '/404' },
  loginData: {
    url: 'auth/login',
    method: 'post',
    redirect: '/',
    fetchUser: false,
    staySignedIn: true,
  },
  fetchData: {
    url: 'auth/me',
    method: 'POST',
    enabled: false,
  },
  refreshData: {
    url: 'auth/refresh',
    method: 'POST',
    enabled: false,
  },
  parseUserData: (data) => data,
};
