import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
    meta: {
      auth: false,
    },
  },
  {
    path: '/clients',
    name: 'ClientsList',
    component: () => import('../views/Clients/ClientsList.vue'),
    meta: {
      auth: true,
    },
  },
  {
    path: '/clients/create',
    name: 'ClientsCreate',
    component: () => import('../views/Clients/ClientsCreate.vue'),
    meta: {
      auth: true,
    },
  },
  {
    path: '/clients/:id',
    name: 'ClientsShow',
    component: () => import('../views/Clients/ClientsShow.vue'),
    meta: {
      auth: true,
    },
  },
  {
    path: '/clients/:id/edit',
    name: 'ClientsEdit',
    component: () => import('../views/Clients/ClientsEdit.vue'),
    meta: {
      auth: true,
    },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
